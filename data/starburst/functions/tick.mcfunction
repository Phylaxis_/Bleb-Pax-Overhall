execute as @a store result score @s arrowCount run clear @s minecraft:arrow 0

execute as @a[nbt={SelectedItem:{id:"minecraft:crossbow",tag:{starburst:1,Charged:0b}}}] run clear @s arrow 1
execute as @a[nbt={SelectedItem:{id:"minecraft:crossbow",tag:{starburst:1,Charged:0b}}},scores={arrowCount=1..}] run replaceitem entity @s weapon.mainhand crossbow{display:{Name:'{"text":"Starburst","color":"light_purple","italic":false}',Lore:['{"text":"Legendary Weapon","color":"gold","italic":false}']},HideFlags:32,Unbreakable:1b,starburst:1,Enchantments:[{}],ChargedProjectiles:[{id:"minecraft:arrow",Count:1b},{},{}],Charged:1b} 1

replaceitem entity @a[gamemode=creative,nbt={SelectedItem:{id:"minecraft:crossbow",tag:{starburst:1,Charged:0b}}}] weapon.mainhand crossbow{display:{Name:'{"text":"Starburst","color":"light_purple","italic":false}',Lore:['{"text":"Legendary Weapon","color":"gold","italic":false}']},HideFlags:32,Unbreakable:1b,starburst:1,Enchantments:[{}],ChargedProjectiles:[{id:"minecraft:arrow",Count:1b},{},{}],Charged:1b} 1

