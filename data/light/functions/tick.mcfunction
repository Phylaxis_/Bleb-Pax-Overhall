execute as @a at @s run function light:get_light_level

execute as @a[tag=LightValue] run title @s actionbar ["",{"score":{"name":"@s","objective":"light"},"color":"black"}]

execute as @a[tag=Player,scores={light=..3}] at @s run effect give @s blindness 9999 0 true
execute as @a[tag=Player,scores={light=4..}] at @s run effect clear @s blindness