gamerule sendCommandFeedback false

execute if entity @s[tag=Caster] run particle reverse_portal ~ ~ ~ 0 0 0 0.1 1 force
execute if entity @s[tag=Caster] run particle enchant ~ ~ ~ 0 0 0 0 1 force

execute if entity @s[tag=!Caster] run playsound minecraft:block.beacon.power_select player @a ~ ~ ~ 0.3 2

tag @s[tag=!Caster] add Caster

execute as @e[dx=0,tag=!Caster] positioned ~-0.99 ~-0.99 ~-0.99 if entity @s[dx=0] run kill @s

execute as @s[tag=Caster] run scoreboard players remove @s range 1

execute if entity @s[tag=Caster,scores={range=1..}] if block ~ ~ ~ air positioned ^ ^ ^0.5 run function debug:hex

tag @s[tag=Caster] remove Caster

tag @s add Cast
