gamerule sendCommandFeedback false
fill ~3 ~-1 ~3 ~-3 ~5 ~-3 air
fill ~-2 ~-1 ~-2 ~2 ~-1 ~2 minecraft:quartz_bricks
fill ~2 ~-1 ~3 ~-2 ~-1 ~3 minecraft:smooth_quartz_stairs[facing=north]
fill ~2 ~-1 ~-3 ~-2 ~-1 ~-3 minecraft:smooth_quartz_stairs[facing=south]
fill ~-3 ~-1 ~2 ~-3 ~-1 ~-2 minecraft:smooth_quartz_stairs[facing=east]
fill ~3 ~-1 ~2 ~3 ~-1 ~-2 minecraft:smooth_quartz_stairs[facing=west]
setblock ~-3 ~-1 ~-3 minecraft:chiseled_quartz_block
setblock ~3 ~-1 ~-3 minecraft:chiseled_quartz_block
setblock ~-3 ~-1 ~3 minecraft:chiseled_quartz_block
setblock ~3 ~-1 ~3 minecraft:chiseled_quartz_block
fill ~-3 ~ ~-3 ~-3 ~4 ~-3 minecraft:quartz_pillar
fill ~3 ~ ~-3 ~3 ~4 ~-3 minecraft:quartz_pillar
fill ~-3 ~ ~3 ~-3 ~4 ~3 minecraft:quartz_pillar
fill ~3 ~ ~3 ~3 ~4 ~3 minecraft:quartz_pillar
setblock ~-3 ~5 ~-3 minecraft:chiseled_quartz_block
setblock ~3 ~5 ~-3 minecraft:chiseled_quartz_block
setblock ~-3 ~5 ~3 minecraft:chiseled_quartz_block
setblock ~3 ~5 ~3 minecraft:chiseled_quartz_block
fill ~3 ~5 ~-2 ~3 ~5 ~2 minecraft:quartz_pillar[axis=z]
fill ~-3 ~5 ~-2 ~-3 ~5 ~2 minecraft:quartz_pillar[axis=z]
fill ~2 ~5 ~-3 ~-2 ~5 ~-3 minecraft:quartz_pillar[axis=x]
fill ~2 ~5 ~3 ~-2 ~5 ~3 minecraft:quartz_pillar[axis=x]
fill ~1 ~5 ~2 ~-1 ~5 ~2 minecraft:quartz_stairs[half=top,facing=south]
fill ~1 ~5 ~-2 ~-1 ~5 ~-2 minecraft:quartz_stairs[half=top,facing=north]
fill ~-2 ~5 ~1 ~-2 ~5 ~-1 minecraft:quartz_stairs[half=top,facing=west]
fill ~2 ~5 ~1 ~2 ~5 ~-1 minecraft:quartz_stairs[half=top,facing=east]
setblock ~2 ~5 ~2 minecraft:quartz_stairs[half=top,shape=inner_left,facing=south]
setblock ~-2 ~5 ~2 minecraft:quartz_stairs[half=top,shape=inner_left,facing=west]
setblock ~2 ~5 ~-2 minecraft:quartz_stairs[half=top,shape=inner_left,facing=east]
setblock ~-2 ~5 ~-2 minecraft:quartz_stairs[half=top,shape=inner_left,facing=north]
fill ~1 ~5 ~1 ~-1 ~5 ~-1 minecraft:white_stained_glass
setblock ~3 ~4 ~2 minecraft:quartz_stairs[half=top,facing=south]
setblock ~-3 ~4 ~2 minecraft:quartz_stairs[half=top,facing=south]
setblock ~3 ~4 ~-2 minecraft:quartz_stairs[half=top,facing=north]
setblock ~-3 ~4 ~-2 minecraft:quartz_stairs[half=top,facing=north]
setblock ~2 ~4 ~3 minecraft:quartz_stairs[half=top,facing=east]
setblock ~2 ~4 ~-3 minecraft:quartz_stairs[half=top,facing=east]
setblock ~-2 ~4 ~3 minecraft:quartz_stairs[half=top,facing=west]
setblock ~-2 ~4 ~-3 minecraft:quartz_stairs[half=top,facing=west]
setblock ~3 ~4 ~ minecraft:lantern[hanging=true]
setblock ~-3 ~4 ~ minecraft:lantern[hanging=true]
setblock ~ ~4 ~3 minecraft:lantern[hanging=true]
setblock ~ ~4 ~-3 minecraft:lantern[hanging=true]
setblock ~-2 ~ ~2 minecraft:black_bed[part=head,facing=south]
setblock ~-2 ~ ~1 minecraft:black_bed[part=foot,facing=south]
setblock ~-2 ~ ~3 minecraft:birch_wall_sign[facing=south]
data merge block ~-2 ~ ~3 {Text1:'{"text":"","clickEvent":{"action":"run_command","value":"execute if entity @s[gamemode=creative] run fill ~-1 ~-1 ~ ~5 ~5 ~-6 air"}}',Text2:'{"text":"","clickEvent":{"action":"run_command","value":"execute if entity @s[gamemode=creative] run kill @e[type=item,distance=0..5]"}}'}
setblock ~-2 ~ ~-1 chest[facing=east,type=right]{Items:[{Slot:0b,id:"minecraft:crossbow",Count:1b,tag:{display:{Name:'{"text":"Starburst","color":"light_purple","italic":false}',Lore:['{"text":"Legendary Weapon","color":"gold","italic":false}']},HideFlags:32,Unbreakable:1b,starburst:1,Enchantments:[{}],ChargedProjectiles:[{id:"minecraft:arrow",Count:1b},{},{}],Charged:1b}},{Slot:6b,id:"minecraft:bow",Count:1b},{Slot:7b,id:"minecraft:tipped_arrow",Count:1b,tag:{display:{Name:'{"text":"TNT Arrow","color":"dark_red","italic":false}'},HideFlags:32,CustomPotionEffects:[{Id:27b,Amplifier:21b,Duration:1}],CustomPotionColor:16711680}},{Slot:8b,id:"minecraft:tipped_arrow",Count:1b,tag:{display:{Name:'{"text":"Lightning Bolt","color":"#FFFF00","italic":false}'},HideFlags:32,CustomPotionEffects:[{Id:27b,Amplifier:23b,Duration:1}],CustomPotionColor:16776960}},{Slot:16b,id:"minecraft:tipped_arrow",Count:1b,tag:{display:{Name:'{"text":"Blaze Arrow","color":"#FF8800","italic":false}'},HideFlags:32,CustomPotionEffects:[{Id:27b,Amplifier:22b,Duration:1}],CustomPotionColor:16746496}},{Slot:17b,id:"minecraft:tipped_arrow",Count:1b,tag:{display:{Name:'{"text":"Venom Arrow","color":"#00FF00","italic":false}'},HideFlags:32,CustomPotionEffects:[{Id:27b,Amplifier:24b,Duration:1}],CustomPotionColor:65280}},{Slot:26b,id:"minecraft:tipped_arrow",Count:1b,tag:{display:{Name:'{"text":"Web Arrow","italic":false}'},HideFlags:32,CustomPotionEffects:[{Id:27b,Amplifier:25b,Duration:1}],CustomPotionColor:16777215}}]} replace
setblock ~-2 ~ ~-2 chest[facing=east,type=left]{Items:[{Slot:17b,id:"minecraft:structure_void",Count:1b},{Slot:18b,id:"minecraft:splash_potion",Count:1b,tag:{display:{Name:'{"text":"Night Vision","italic":false}'},CustomPotionEffects:[{Id:16b,Amplifier:0b,Duration:72000,ShowParticles:0b}],CustomPotionColor:16777215}},{Slot:25b,id:"minecraft:barrier",Count:1b},{Slot:26b,id:"minecraft:name_tag",Count:1b}]} replace
setblock ~2 ~ ~ minecraft:command_block[facing=west]
setblock ~2 ~ ~2 minecraft:anvil[facing=south]
setblock ~2 ~ ~-2 minecraft:ender_chest[facing=west]
kill @e[type=item,distance=0..5]
tag @s add Cast
